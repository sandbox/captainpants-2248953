<?php

class related_taxonomy_contextual_filter_plugin_argument_default_related_taxonomy extends views_plugin_argument_default {
  function init(&$view, &$argument, $options) {
    parent::init($view, $argument, $options);

    // Convert legacy vids option to machine name vocabularies.
    if (!empty($this->options['vids'])) {
      $vocabularies = taxonomy_get_vocabularies();
      foreach ($this->options['vids'] as $vid) {
        if (isset($vocabularies[$vid], $vocabularies[$vid]->machine_name)) {
          $this->options['vocabularies'][$vocabularies[$vid]->machine_name] = $vocabularies[$vid]->machine_name;
        }
      }
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['vocabularies'] = array('default' => array());

    return $options;
  }

  function options_form(&$form, &$form_state) {

    $options = array();
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabularies as $voc) {
      $options[$voc->vid] = check_plain($voc->name);
    }

    $form['vocabularies'] = array(
      '#prefix' => '<div><div id="edit-options-vids">',
      '#suffix' => '</div></div>',
      '#type' => 'checkboxes',
      '#title' => t('Vocabularies'),
      '#description' => t('Be sure to check the "Allow multiple values" option in the more section'),
      '#options' => $options,
      '#default_value' => $this->options['vocabularies'],
    );
  }

  function options_submit(&$form, &$form_state, &$options = array()) {
    // Filter unselected items so we don't unnecessarily store giant arrays.
    $options['vocabularies'] = array_filter($options['vocabularies']);
  }

  function get_argument() {

    foreach (range(1, 3) as $i) {
      $node = menu_get_object('node', $i);
      if (!empty($node)) {
        
        $related_nids = array();
        
        foreach ($this->options['vocabularies'] as $vid => $vocabulary) {
          $terms = taxonomy_get_tree($vid);
          
          foreach ($terms as $term) {
            $nids_in_term = taxonomy_select_nodes($term->tid, FALSE);
            if (in_array($node->nid, $nids_in_term)) {
              array_push($related_nids, $nids_in_term);
            }
          }
        }
        
        $related_nids = array_unique(call_user_func_array('array_merge', $related_nids));
        // Remove the current node from the related nids.
        $related_nids = array_diff($related_nids, array($node->nid));
        
        return implode(',', $related_nids);
      }
      else {
        return $node->nid;
      }

    }
  }
}
